%% Machine Learning: Test a Collaborative Filtering machine
%


%% ============== Entering ratings for a new users ===============
%  Before we will train the collaborative filtering model, we will first
%  add ratings that correspond to a new user that we just observed. This
%  part of the code will also allow you to put in your own ratings for the
%  movies in our dataset!
%
movieList = loadMovieList();

%  Initialize my ratings
my_ratings1 = zeros(1682, 1);
my_ratings2 = zeros(1682, 1);
my_ratings3 = zeros(1682, 1);

% Check the file movie_idx.txt for id of each movie in our dataset

% We have selected a few movies we liked / did not like and the ratings we
% gave are as follows:
my_ratings1(1) = 4;
my_ratings1(98) = 2;
my_ratings1(7) = 3;
my_ratings1(12)= 5;
my_ratings1(54) = 4;
my_ratings1(64)= 5;
my_ratings1(66)= 3;
my_ratings1(69) = 5;
my_ratings1(183) = 4;
my_ratings1(226) = 5;
my_ratings1(355)= 5;

my_ratings2(100) = 4;
my_ratings2(200) = 2;
my_ratings2(300) = 3;
my_ratings2(400)= 5;
my_ratings2(500) = 4;
my_ratings2(600)= 5;
my_ratings2(700)= 3;
my_ratings2(800) = 5;
my_ratings2(900) = 4;
my_ratings2(1100) = 5;
my_ratings2(1111)= 5;

fprintf('\n\nNew user1 ratings:\n');
for i = 1:length(my_ratings1)
    if my_ratings1(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings1(i), ...
                 movieList{i});
    end
end

fprintf('\n\nNew user2 ratings:\n');
for i = 1:length(my_ratings)
    if my_ratings2(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings2(i), ...
                 movieList{i});
    end
end

fprintf('\n\nNew user3 ratings:\n');
for i = 1:length(my_ratings)
    if my_ratings3(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings3(i), ...
                 movieList{i});
    end
end
fprintf('\nProgram paused. Press enter to continue.\n');
pause;


%% ================== Learning Movie Ratings ====================
%  Now, you will train the collaborative filtering model on a movie rating 
%  dataset of 1682 movies and 943 users
%

fprintf('\nTraining collaborative filtering...\n');

%  Load data
load('ex8_movies.mat');

%  Y is a 1682x943 matrix, containing ratings (1-5) of 1682 movies by 
%  943 users
%
%  R is a 1682x943 matrix, where R(i,j) = 1 if and only if user j gave a
%  rating to movie i

%  Add our own ratings to the data matrix
Y = [my_ratings3 my_ratings2 my_ratings1 Y];
R = [(my_ratings3 ~= 0) (my_ratings2 ~= 0) (my_ratings1 ~= 0) R];

%  Normalize Ratings
[Ynorm, Ymean] = normalizeRatings(Y, R);

%  Useful Values
num_users = size(Y, 2);
num_movies = size(Y, 1);
num_features = 10;

% Set Initial Parameters (Theta, X)
X = randn(num_movies, num_features);
Theta = randn(num_users, num_features);

initial_parameters = [X(:); Theta(:)];

% Set options for fmincg
options = optimset('GradObj', 'on', 'MaxIter', 100);

% Set Regularization
lambda = 10;
theta = fmincg (@(t)(cofiCostFunc(t, Ynorm, R, num_users, num_movies, ...
                                num_features, lambda)), ...
                initial_parameters, options);

% Unfold the returned theta back into X and Theta
X = reshape(theta(1:num_movies*num_features), num_movies, num_features);
Theta = reshape(theta(num_movies*num_features+1:end), ...
                num_users, num_features);

fprintf('Recommender system learning completed.\n');

fprintf('\nProgram paused. Press enter to continue.\n');
pause;

%% ================== Recommendation for users ====================
%  After training the model, you can now make recommendations by computing
%  the predictions matrix.
%

p = X * Theta';
movieList = loadMovieList();

% For User 3
my_predictions3 = p(:,1) + Ymean;

[r, ix] = sort(my_predictions3, 'descend');
fprintf('\nTop recommendations for User 3:\n');
for i=1:10
    j = ix(i);
    fprintf('Predicting rating %.1f for movie %s\n', my_predictions3(j), ...
            movieList{j});
end

fprintf('\n\nOriginal ratings provided by user 3:\n');
for i = 1:length(my_ratings)
    if my_ratings3(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings3(i), ...
                 movieList{i});
    end
end

% For User 2
my_predictions2 = p(:,2) + Ymean;

[r, ix] = sort(my_predictions2, 'descend');
fprintf('\nTop recommendations for user 2:\n');
for i=1:10
    j = ix(i);
    fprintf('Predicting rating %.1f for movie %s\n', my_predictions2(j), ...
            movieList{j});
end

fprintf('\n\nOriginal ratings provided:\n');
for i = 1:length(my_ratings)
    if my_ratings2(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings2(i), ...
                 movieList{i});
    end
end

% For User 1
my_predictions1 = p(:,3) + Ymean;

[r, ix] = sort(my_predictions1, 'descend');
fprintf('\nTop recommendations for user 1:\n');
for i=1:10
    j = ix(i);
    fprintf('Predicting rating %.1f for movie %s\n', my_predictions1(j), ...
            movieList{j});
end

fprintf('\n\nOriginal ratings provided:\n');
for i = 1:length(my_ratings)
    if my_ratings1(i) > 0 
        fprintf('Rated %d for %s\n', my_ratings1(i), ...
                 movieList{i});
    end
end