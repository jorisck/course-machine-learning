function J_value = runkMeansToFindK(X, max_iters)
close all;

%Some values for K. X need to have m>= 128 in this case to avoid some errors int the init function
K_vec = [2 4 8 16 32 64 128]';

J_value = zeros(length(K_vec), 1);


for i=1:length(K_vec)
	K_vec(i)
	initial_centroids = kMeansInitCentroids(X, K_vec(i));
	[centroids, idx] = runkMeans(X, initial_centroids, max_iters);
	%J_temp to calculte J for one cluster
	J_temp=0;
	for j=1:K_vec(i)
		%choose rigth cluster by using idx==cluster I have now a vector with only 1 and 0
		vectors_with_1=(idx == j);

		%X_temp = X with only the value of the samples in the cluster otherwise 0 for the others
		%I choose one centroid. I substract it on X.
		% By multiplying with vectors_with_1 I keep only the samples in the cluster otherwise 0 for the others
		X_temp =(X-centroids(j,:)).*vectors_with_1;

		%I compute J_temp
		J_temp=J_temp + sum(sum(X_temp.^2 ));
	end
	J_value(i)=J_temp;
	J_value
end

J_value
[value kmeans]= min(J_value);

plot(K_vec , J_value,'x',"markersize", 10);
xlabel ("K-value");
ylabel ("J");
title ("Choosing the value of K: elbow method");

fprintf('Program paused. Press enter to continue.\n');
pause;



