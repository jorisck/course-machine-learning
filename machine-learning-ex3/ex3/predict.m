function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.

% Add ones to the X data matrix
X = [ones(m, 1) X];

%[a*m] = [a*n+1] * [n+1*m]
Z2=Theta1*X';

%[a*m]
A2 = sigmoid(Z2);

% Add ones above each colomun in the A2 data matrix 
%(each column represents an example)
A2 = [ones(1,m); A2 ];

%[K*m] = [K*a+1]*[a+1*m]
Z3=Theta2*A2;
hypothesis = sigmoid(Z3)

%probability = column_max_values
[column_max_values row_max_indexes] = max(hypothesis);

p= row_max_indexes';



% =========================================================================


end
