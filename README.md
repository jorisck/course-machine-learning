# Series of courses from coursera. 
Many thanks to Prof. Andrew Ng and his team for these great courses.

This repository has only an academic purpose and serve also as a reference 
for me if necessary in the future. I'm fully available if you wish  to discuss 
about these courses and/or about machine learning issues.

Reminder: Please only use it as a reference.

## Note 1:
In all the exercices I try to vectorize as much as possible, avoiding to use a For-loop.

## Note 2:

### In ex6 - week 7:

I created a new file "svmPredictMyMail". Inside you cand find the
function svmPredictMyMail(filename,model). If you run this file with
the name of your spam (or not) text file and the model created from the ex6_spam script,
it will predict whether the content is likely a spam or not

### ex7 - week 8:
After running your ex7 script, you can use my "runkMeansOnMyImages" function  
to compress your own images.
This function takes the image filename, K and max_iter.
If the kMeans seems slow, try resizing your image file.
Koala.jpg and Penguins come from Windows library.

I also implemented "runkMeansToFindK" function to find the best K to use
given a max_iter and some data. It draw the graph at the end. 
We can use the elbow method here.

### ex8 - week 9:
After running ex8_cofi, you can run my script "PredictMoviesFor3USers"
For 1 user I didn't define ratings. So he will have the movies with high rate 
from others users.
